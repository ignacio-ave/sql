
-- Creación de tablas

CREATE TABLE Cliente
(
  Rut INT NOT NULL,
  Direccion VARCHAR(50) NOT NULL,
  Nombre VARCHAR(30) NOT NULL,
  Apellido VARCHAR(30) NOT NULL,
  PRIMARY KEY (Rut)
);

CREATE TABLE Auto
(
  Modelo VARCHAR NOT NULL,
  Color VARCHAR NOT NULL,
  Marca VARCHAR NOT NULL,
  Patente INT NOT NULL,
  Estado VARCHAR NOT NULL,
  Precio FLOAT NOT NULL,
  Rut INT ,
  PRIMARY KEY (Patente),
  FOREIGN KEY (Rut) REFERENCES Cliente(Rut)
);

CREATE TABLE Servicio
(
  Id_Servicio INT NOT NULL,
  Descripcion_del_servicio VARCHAR(40) NOT NULL,
  Precio FLOAT NOT NULL,
  PRIMARY KEY (Id_Servicio)
);

CREATE TABLE Ticket_de_servicio
(
  Id_Ticket_de_servicio INT NOT NULL,
  Fecha DATE NOT NULL,
  Id_Servicio INT NOT NULL,
  Rut INT NOT NULL,
  PRIMARY KEY (Id_Ticket_de_servicio),
  FOREIGN KEY (Id_Servicio) REFERENCES Servicio(Id_Servicio),
  FOREIGN KEY (Rut) REFERENCES Cliente(Rut)
);

CREATE TABLE Vendedor
(
  Id_vendedor INT NOT NULL,
  Apellido VARCHAR(30) NOT NULL,
  Nombre VARCHAR(30) NOT NULL,
  PRIMARY KEY (Id_vendedor)
);

CREATE TABLE Mecanico
(
  Id_Mecanico INT NOT NULL,
  Nombre VARCHAR(30) NOT NULL,
  Apellido VARCHAR(30) NOT NULL,
  PRIMARY KEY (Id_Mecanico)
);

CREATE TABLE Factura
(
  Fecha DATE NOT NULL,
  Id_Factura INT NOT NULL,
  Patente INT NOT NULL,
  Id_vendedor INT NOT NULL,
  Rut INT NOT NULL,
  PRIMARY KEY (Id_Factura),
  FOREIGN KEY (Patente) REFERENCES Auto(Patente),
  FOREIGN KEY (Id_vendedor) REFERENCES Vendedor(Id_vendedor),
  FOREIGN KEY (Rut) REFERENCES Cliente(Rut)
);

CREATE TABLE Mecanicos_en_el_servicio
(
  Id_Mecanico INT NOT NULL,
  Id_Ticket_de_servicio INT NOT NULL,
  FOREIGN KEY (Id_Mecanico) REFERENCES Mecanico(Id_Mecanico),
  FOREIGN KEY (Id_Ticket_de_servicio) REFERENCES Ticket_de_servicio(Id_Ticket_de_servicio)
);

