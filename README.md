
# Tarea N°2 - ICD3350-1 - Base de Datos (1S2024) 

![Logo de la Escuela de Ingeniería Informática PUCV](https://i.imgur.com/CxzeZbr.png) 

## Participantes

- Ignacio Astorga
- Vicente Hernández

## Descripción del Proyecto

Este proyecto consiste en la realización de la Tarea N°2 del curso ICD3350-1 - Base de Datos (1S2024). Aborda la especificación de requerimientos de un concesionario de autos y la creación de un Modelo Entidad-Relación (MER) que cumpla con dichos requerimientos.

## Especificación de requerimientos

Un concesionario de autos vende autos nuevos, autos usados y además posee un servicio técnico que proporciona algunos servicios. Las reglas del negocio son las siguientes:

- Un vendedor puede vender muchos autos, pero un auto es vendido sólo por un vendedor.
- Un cliente puede comprar muchos autos pero cada auto es comprado sólo por un cliente.
- Un vendedor genera una única factura por cada auto que vende.
- Cualquier persona puede llevar su auto al servicio técnico del concesionario y aunque no haya comprado un auto será registrado como cliente.
- Cada visita al servicio técnico genera un ticket de servicio.
- Un auto puede ser atendido por muchos mecánicos y a su vez cada mecánico puede trabajar en varios autos.

Planteamos un Modelo Entidad-Relación (MER) que permite responder al menos los siguientes requerimientos:

1. Cuáles son los mecánicos que han realizado la mayor cantidad de trabajos durante el último año?
2. Cuál es el servicio que el concesionario realiza de manera más frecuente?
3. Cuál es el top 3 de los vendedores de autos?
4. Para un cliente determinado, cuál es el detalle (auto, tipo de servicio, mecánicos, valor, etc.) de los servicios realizados sobre su(s) vehículo(s)?
5. Proponga dos consultas que involucren al menos JOIN y/o GROUP BY.
